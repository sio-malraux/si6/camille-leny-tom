#include <iostream>
#include <cstring>
#include <libpq-fe.h>
#include "iomanip"

void f_ligne_de_separation(int max,int nbcolonne, const char * ligne_de_separation)
{
        for(int nbseparation = 0; nbseparation < nbcolonne * (max + 1); ++nbseparation){
        	std::cout << ligne_de_separation;
        }
	std::cout <<ligne_de_separation;
        std::cout << std::endl;
}

int main()
{
  PGresult *res;
  ExecStatusType status_return;
  PGPing code_retour_ping;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432");
  PGconn *conn;
  conn = PQconnectdb ("host=postgresql.bts-malraux72.net port=5432 dbname=c.dupuy user=c.dupuy connect_timeout=10");
  char *host = PQhost(conn);  // Renvoie le nom d'hôte du serveur
  char *user = PQuser(conn);  // Renvoie le nom d'utilisateur utilisé pour la connexion
  char *pass = PQpass(conn);  // Renvoie le mot de passe utilisé pour la connexion
  char *bdd = PQdb(conn); // Renvoie le nom de la base de données de la connexion
  char *porttcp = PQport(conn); // Renvoie le numéro du port utilisé
  PQsslInUse(conn); // Renvoie le chiffrement SSL
  const char *param = "server_encoding"; // Création d'un varible afin d'obtenir la version l'encodage
  const char *encodage = PQparameterStatus (conn, param); // Renvoie le protocole d'encodage
  int bibliolibpq = PQlibVersion ();// Renvois la version libpq en cours d'utlisation
  int versionP = PQprotocolVersion (conn); // Renvois la version du protocole
  int versionS = PQserverVersion (conn); // Renvois la version du serveur


  if(code_retour_ping == PQPING_OK)//Verifie que le serveur est accessible
  {
    std::cout << "La connexion au serveur de base de données à été établie avec succès"  << std::endl;


    if (PQstatus(conn) == CONNECTION_OK) //Vérifie que la connexion au serveur est OK
    {
     std::cout << "La connexion au serveur de base '"  << host <<"' a été établie avec les paramètres suivants :  "<< std::endl;
     std::cout << "\t * utilisateur : "<< user << std::endl;
     std::cout << "\t * mot de passe  : ";
     for (int i = 0;i< sizeof(pass); ++i)
     {
	std::cout<<"*";
     }
     std::cout<<std::endl;
     std::cout << "\t * base de données : "<<bdd<< std::endl;
     std::cout << "\t * port TCP : "<< porttcp << std::endl;
     std::cout << "\t * chifremment ssl : " <<(PQsslInUse(conn)?"true" : "false") << std::endl;
     std::cout << "\t * encodage : "<< encodage << std::endl;
     std::cout << "\t * version du protocole : "<< versionP << std::endl;
     std::cout << "\t * version du serveur : "<< versionS << std::endl;
     std::cout << "\t * version de la bibliothèque 'libpq' du client  : "<< bibliolibpq << std::endl;
    }
   //La requête envoyé au serveur contenant la requête SQL
   res = PQexec(conn,"SELECT \"Animal\".\"id\" ,\"Animal\".\"nom\", \"Animal\".\"sexe\", \"Animal\".\"date_naissance\", \"Animal\".\"commentaires\", \"Race\".\"nom\", \"Race\".\"description\" FROM \"si6\".\"Race\" INNER JOIN \"Animal\" ON \"Race\".\"id\" = \"Animal\".\"race_id\" WHERE \"Race\".\"nom\" = 'Singapura' and \"sexe\" = 'Femelle'");

   //Reçois le résultat de la requête
   status_return = PQresultStatus(res);

   // La requête s'execute
   if(status_return == PGRES_TUPLES_OK)
   {
    std::cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)."  << std::endl;

    int ligne = PQntuples(res);
    int colonne = PQnfields(res);
    const char *separation = "|"; //constante pour séparer les champs
    const char *ligne_de_separation="-";// constante pour séparer l'en-tête et les données
    int max=0;
    int compteur = 0;

    //Normalisation de la largeur des champs à la longeur du nom de champ le plus long (question 8)
    for (int b= 0; b <colonne; ++b)
    {
	char *entete=PQfname(res, b);
	if (strlen(entete) > max)
	{
		max = strlen(entete);
        }
    }

    f_ligne_de_separation(max,colonne,ligne_de_separation);
    //Boucle pour afficher chacunes ds en-têtes de la requête SQL
    for (int h = 0; h < colonne; ++h)
    {
	char *entete=PQfname(res, h);//Varaible contenant les en-têtes de la requête SQL
 	std::cout<<separation<<std::setw(max)<<std::left<<entete;//Permet de placer le separatuer "|" - d'espacer les champs - (std::left)place les champs a gauche
    }
    std::cout<<separation;//Affiche un dernier "|" pour ferme le tableau de l'affichage
    std::cout << std::endl;

    //Ligne de spération entre l'en-tête et les données pour former le table
    f_ligne_de_separation(max,colonne,ligne_de_separation);

    //Affichage des données de la requêtes SQL
    for (int i = 0; i < ligne; i++)
    {
     for (int f = 0 ; f < colonne; f++)
     {
	 char *donnee =PQgetvalue(res, i, f);//Variables contenant les don,eés de la requête SQL
	 for (int taille = 0 ; donnee [taille] != '\0'; taille++)
	 {
		compteur++;
	 }
	 if (compteur > max)
 	 {
		donnee[max]= '\0';
		for (int taille = max-3; taille <max; taille++)
		{
			donnee[taille]='.';
		}
	 }
	 compteur = 0;
         std::cout<<separation<<std::setw(max)<<std::left<<PQgetvalue(res,i,f);//Permet de placer le separatuer "|" - d'espacer les champs - (std::left)place les champs a gauche
    }
    std::cout<<separation<<std::endl;
   }

      /*   if (strlen(donnee)>max)
	 {
	  for(int e= 0; e < (max - 3); e++)
	  {
	    //donnee.resize(max-3);
	   // char taille = [max-3];
	    std::cout<<separation<<std::setw(max)<<std::left<<"aaaaaaaabbbbbbbbbbbbbbbbbbbb";
	  }
	  std::cout<<"...";
	 }
	 else
	 {
         std::cout<<separation<<std::setw(max)<<std::left<<donnee;//Permet de placer le separatuer "|" - d'espacer les champs - (std::left)place les champs a gauche
	 }
     }
    std::cout <<separation;
    std::cout<<std::endl;
    }*/

    //Ligne de sépration en dessous des donées pour former le tableau
    f_ligne_de_separation(max,colonne,ligne_de_separation);

    //Ligne affichant le nombre de requête SQl retourné
    std::cout<<"L'exécution de la requête SQL àa retourné "<< ligne <<" enregistrements"<<std::endl;
    }


   // Tous les else if représente la gestion des erreurs possibles
   else if(status_return == PGRES_EMPTY_QUERY)
   {
     std::cout << "La chaîne envoyée au serveur était vide." << std::endl;
   }
   else if(status_return == PGRES_COMMAND_OK)
   {
     std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
   }
   else if(status_return == PGRES_COPY_OUT)
   {
     std::cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
   }
   else if(status_return == PGRES_COPY_IN)
   {
     std::cout << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
   }
   else if(status_return == PGRES_BAD_RESPONSE)
   {
     std::cout << "La réponse du serveur n'a pas été comprise." << std::endl;
   }
   else if(status_return == PGRES_NONFATAL_ERROR)
   {
     std::cout << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
   }
   else if(status_return == PGRES_FATAL_ERROR)
   {
     std::cout << "Une erreur fatale est survenue." << std::endl;
   }
   else if(status_return == PGRES_COPY_BOTH)
   {
     std::cout << "" << std::endl;
   }
   else if(status_return == PGRES_SINGLE_TUPLE)
   {
     std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode simple ligne a été sélectionné pour cette requête" << std::endl;
   }
   else
   {
     std::cout <<"La connexion à échoué et nous ne connaissons pas l'erreur dû a cet echec.";
   }
  }

  // Si le PQPING_OK ne fonctionne pas alors le serveur est inaccessibles
  else
  {
  std::cerr<<"Malheursement le serveur n'est pas joignable"<< std::endl;
  }



  return 0;
}
